# React project

This is a simple project that demonstrates how to use
[R2Devops.io](https://r2devops.io) to build the full CI/CD pipeline for a React project.

- **Templates used:**
    - [NPM install](https://r2devops.io/marketplace/gitlab/r2devops/hub/npm_install): Install dependencies listed in package.json and expose node_modules as cache to other jobs
    - [Gitleaks](https://r2devops.io/marketplace/gitlab/r2devops/hub/gitleaks): A secret detection job, using gitleaks to alert on secrets (passwords, api keys…) pushed in your code
    - [NPM lint](https://r2devops.io/marketplace/gitlab/r2devops/hub/npm_lint): Run the predefined lint script in the package.json which will check your code quality with eslint
    - [NPM test](https://r2devops.io/marketplace/gitlab/r2devops/hub/npm_test): Run the predefined test script in the package.json which will execute test file with Jest
    - [Gitleaks](https://r2devops.io/marketplace/gitlab/r2devops/hub/gitleaks): check for secrets leaked in repository
    - [NPM build](https://r2devops.io/marketplace/gitlab/r2devops/hub/npm_build): This job will build the project using the script from the package.json file
    - [Pages](https://r2devops.io/marketplace/gitlab/r2devops/hub/pages): Deploy the project on a static website using GitLab Pages
- **CI configuration:** [.gitlab-ci.yml](.gitlab-ci.yml)
- **Pipelines:** [CI/CD page](https://gitlab.com/r2devops/demos/react/-/pipelines)
- **Results:**
    - [React project deployed on GitLab Pages](https://r2devops.gitlab.io/demos/react/)